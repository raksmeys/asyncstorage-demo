import React, { useState } from 'react'
import { Text, View, StyleSheet, TextInput, Dimensions, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';

export default Login = ({ navigation }) => {
    const [username, setUsername] = useState('')
    const [errorUsername, setError] = useState('')
    const storeData = async (value) => {
        try {
            await AsyncStorage.setItem('@name', value)
        } catch (e) {
            // saving error
        }
    }

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem('@name')
            if (value !== null) {
                // value previously stored
                console.log(value)
            }
        } catch (e) {
            // error reading value
        }
    }

    const handleLogin = async () => {
         
        console.log('usernae', username)
        if (username == "") {
            setError('*Require')
        } else {
            setError('')
            // store data
            // get data => navigate to home screen  (data)
            await AsyncStorage.setItem('@name', username)
            const value = await AsyncStorage.getItem('@name')

            console.log(value)

            navigation.navigate('Home', {
                name: value
            })
        }
   
    }
    return (
        <View style={styles.container}>
            <Text style={{color: 'red'}}>{errorUsername}</Text>
            <TextInput
                style={styles.inputStyle}
                placeholder="Username"
                onChangeText={(username) => setUsername(username)}
            />
            <TouchableOpacity
                style={styles.btnStyle}
                onPress={() => handleLogin()}
            >
                <Text>
                    Login
                    </Text>
            </TouchableOpacity>

        </View>
    )

}



const WIDTH = Dimensions.get('screen').width
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputStyle: {
        width: WIDTH - 50,
        height: 70,
        borderRadius: 15,
        padding: 10,
        borderColor: 'grey',
        borderWidth: 1,
        margin: 10

    },
    btnStyle: {
        width: WIDTH - 50,
        height: 70,
        borderRadius: 35,
        backgroundColor: 'lightblue',
        justifyContent: 'center',
        alignItems: 'center'
    }
})
